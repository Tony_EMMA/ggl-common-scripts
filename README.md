# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Common utility scripts for GGL

### How do I get set up? ###

It's a shell scripts.

#### Add scripts to your path

Replace **GGL-COMMON-SCRIPTS** with directory which contains scripts.

You can modify your path in order to execute scripts everywhere

`echo "export PATH=$PATH:**GGL-COMMON-SCRIPTS**" >> ~/.profile`

You must add execution permission on scripts:

`chmod +x **GGL-COMMON-SCRIPTS**/ggl-*`

#### Usage

Use option `-h` or `--help` on each script in order to display specific help.

### Scripts

#### ggl-connect-db

Connect to GGL database over cloud SQL proxy. In addition it give the complete PSQL command in order to connect database and data connection like database user, password, name, ...

Some Examples:

* Connect to merch in demo: `ggl-connect-db -d merch -e demo`
* Connect to product in prod: `ggl-connect-db -d product -e pr`
* Connect to offer in preprod: `ggl-connect-db -d offer -e pp`

Example of output:

```
Data connections
  Connected with account: temma.ext@galerieslafayette.com
  Region: europe-west1
  Project: gl-dsi-ecom-pr-ka
  Database: ggl-product-blue-ab
  Port: 5423
  Database name: product
  PSQL: psql "host=127.0.0.1 port=5423 dbname=product user=XXX@galerieslafayette.com sslmode=disable"
```

PSQL is the command to connect database
If you are on int or demo you can have JDBC uri, database user and database password. Example:

```
Data connections
  Connected with account: temma.ext@galerieslafayette.com
  Region: europe-west1
  Project: gl-dsi-ecom-demo-ol
  Database: ggl-product-blue-mc
  Port: 5423
  Database name: product
  User: product
  Password: 545b64269b58c9c6a2844449f2d76385
  JDBC: jdbc:postgresql://localhost:5423/product
  PSQL: PGPASSWORD=545b64269b58c9c6a2844449f2d76385 psql -h 127.0.0.1 -d product -p 5423 -U product
```

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
